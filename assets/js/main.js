var loadLeaderboard;


window.findGetParameter=function(parameterName) {
    var result = null,
    tmp = [];
    location.search
    .substr(1)
    .split("&")
    .forEach(function (item) {
    tmp = item.split("=");
    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
    return result;
};


(function(){

    var skipList = {
		leaderboard: 0,
		newest: 0
	}
    ,   playlistFull = {}
    ,   conversationTemplate
    ,   testRecords = [
        "",
        ""
    ]



    loadPlaylist = function(type){


        if(playlistFull[type])
            return alert("playlist full " + type);

        var $el = $('.pl.' + type)

        if(!$el)
            return console.log(type, 'container not found')

        var skip = skipList[type] || 0

        $.get('/conversation/' + type, {skip: skip})
        .then(function(result){
           // skipList[type] += 5
            result.forEach(function(el,id){
                if(!el.record)
                    el.record = testRecords.pop()

                // console.log(el)
                /*
                $.getJSON('https://graph.facebook.com/v2.4/?fields=share{comment_count}&id=http://debatesclub.com/conversation/'+el.id+'/view',function(res){
                    console.log("res ",res,res.share);
                });
                */
                el.leader="";
                if(type=='leaderboard' && skip==0){
                    console.log('ooooooooo ',type);
                    if(id==0){
                        el.leader = 'top';
                    }
                    if(id==1){
                        el.leader = 'trending';
                    }
                }
                el.date = moment(el.createdAt).format('ll')
                el.TRUMP = _.find(el.queue, function(e){ return e.side == '1'})
                el.CLINTON = _.find(el.queue, function(e){ return e.side == '0'})

                el.TRUMP.rates = 0
                el.CLINTON.rates = 0

                _.each(el.rates, function(r){
                    if(r.queue == el.CLINTON.id){
                        el.CLINTON.rates++
                    } else {
                        el.TRUMP.rates++
                    }
                })
                $el.find(".list").append(conversationTemplate(el))
            })

            if(result.length < 5){
                playlistFull[type] = true
                $el.find('.more').hide()
            }
            DISQUSWIDGETS.getCount({reset: true});
        })

	skipList[type] += 5

    }

    var basicMP3Player = null;

    $(document).ready( function(){

        new Clipboard('[data-clipboard-text]');

        $(".newest-more").click(function(){loadPlaylist('newest')})
    	$(".leaderboard-more").click(function(){loadPlaylist('leaderboard')})
        soundManager.setup({
            // debugMode: false,
          preferFlash: false,
          onready: function() {
            basicMP3Player = new BasicMP3Player();
          }
        });

        $.get('/partials/player.html')
        .then(function(template){
            conversationTemplate = _.template(template)
            loadPlaylist('leaderboard')
            loadPlaylist('newest')
        })
        .catch(function(e) {console.log(e)})


    	$.get('/conversation/total')
        .then(function(data){
            var trumpTotal = _.find(data, function(el){ return el._id == '1'}).total
            ,   clintonTotal = _.find(data, function(el){ return el._id == '0'}).total
            $('.vote-box.trump span').text(trumpTotal)
            $('.vote-box.clinton span').text(clintonTotal)
        })

        $("body").on('click', '.player-container .votes', function(e){
            var $vote = $(e.currentTarget)
            ,   voted = Cookies.getJSON('voted') || []
            if(voted.indexOf($vote.data('conv')) > -1 ){
                return false;
            }

            try{
                yaCounter40526295.reachGoal('rate')
            }catch(e){

            }
            $.post('/rate', {
                conversation: $vote.data('conv'),
                queue: $vote.data('queue'),
            }).then(function(res){
        		voted.push($vote.data('conv'))
                voted.push($vote.data('queue'))
        		Cookies.set('voted', voted)
                $vote.text(parseInt($vote.text()) + 1)
                $vote.addClass('voted')
            }).catch(function(err) {
                console.log("vote err", err)
            })
        })

        var subject = function(e){
            var $el = $(e.currentTarget)
            ,   candidate = $('[name="side"]:checked').val() == "1" ? 'Trump' : 'Clinton'

            if(!(new RegExp('^' + candidate + ' is')).test($el.val()))
                $el.val(candidate + ' is')


        }
        $("body").on('change', '#subj', subject)
        $("body").on('keyup', '#subj', subject)

        $("body").on('submit', '#queueForm', function(e){
            e.preventDefault()

            var data = {}
            ,   errors = []
            $('#queueForm').serializeArray().map(function(item) {
                if ( data[item.name] ) {
                    if ( typeof(data[item.name]) === "string" ) {
                        data[item.name] = [data[item.name]];
                    }
                    data[item.name].push(item.value);
                    if(!item.value && item.required)
                        errors.push(item)
                } else {
                    data[item.name] = item.value;
                }
            });

            if(errors.length){
                return alert("Fill up the form, please")
            }


            $.post('/queue', data)
            .then(function(res){
                console.log(res);

                $('#queueForm').hide();
                $('#vision').hide();
                $('#search').show();

                try{
                	yaCounter40526295.reachGoal('call_init');
                }catch(e){
                	console.log('metrika failed');
                }
            })

        })

    })

})()
