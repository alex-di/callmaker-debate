import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "main": {
        "background": "transparent url(../img/head.png) 50% top no-repeat"
    },
    "queueForm": {
        "paddingTop": 20,
        "paddingRight": 20,
        "paddingBottom": 20,
        "paddingLeft": 20,
        "textAlign": "center",
        "marginTop": 20,
        "marginRight": "auto",
        "marginBottom": 20,
        "marginLeft": "auto",
        "background": "#0a58c4",
        "width": 400,
        "borderRadius": 2,
        "color": "#fff"
    },
    "button": {
        "cursor": "pointer",
        "textDecoration": "none",
        "color": "#fff"
    },
    "header": {
        "background": "transparent url(../img/header.png) 50% -23px no-repeat",
        "color": "#FFF"
    },
    "container": {
        "width": 1033,
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "position": "relative"
    },
    "header site-title": {
        "color": "#fff",
        "textDecoration": "none",
        "transform": "rotate(2deg)",
        "float": "left",
        "fontSize": 33,
        "fontFamily": "\"Myriad Pro\", Myriad, \"Liberation Sans\", \"Nimbus Sans L\", \"Helvetica Neue\", Helvetica, Arial, sans-serif",
        "paddingTop": 42,
        "paddingRight": 33,
        "paddingBottom": 42,
        "paddingLeft": 33,
        "letterSpacing": 0.7
    },
    "header site-title span": {
        "fontSize": 20
    },
    "header logo": {
        "paddingTop": 40,
        "paddingRight": 0,
        "paddingBottom": 40,
        "paddingLeft": 0,
        "float": "right"
    },
    "main content": {
        "color": "#FFF"
    },
    "content h1": {
        "fontSize": 39,
        "textAlign": "center",
        "fontWeight": "bold",
        "paddingTop": 29,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "content h2": {
        "fontSize": 22,
        "lineHeight": 27,
        "paddingTop": 9,
        "paddingRight": 9,
        "paddingBottom": 9,
        "paddingLeft": 9
    },
    "title": {
        "fontSize": 25,
        "fontWeight": "bold"
    },
    "p": {
        "paddingTop": 3,
        "paddingRight": 3,
        "paddingBottom": 3,
        "paddingLeft": 3,
        "fontSize": 18
    },
    "start-box": {
        "border": "1px solid #000",
        "borderRadius": 5,
        "background": "#0a58c4",
        "width": 308,
        "marginTop": 43,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "textAlign": "center",
        "paddingTop": 32,
        "paddingRight": 44,
        "paddingBottom": 27,
        "paddingLeft": 44
    },
    "start-box buttons": {
        "overflow": "hidden",
        "marginTop": 20,
        "paddingTop": 0,
        "paddingRight": 5,
        "paddingBottom": 0,
        "paddingLeft": 5,
        "background": "#1a915a",
        "borderRadius": 5
    },
    "start-box button": {
        "paddingTop": 43,
        "paddingRight": 0,
        "paddingBottom": 43,
        "paddingLeft": 0,
        "height": 134,
        "width": "50%",
        "boxSizing": "border-box",
        "background": "#23c478",
        "float": "left",
        "borderRadius": 5,
        "marginBottom": 7
    },
    "start-box button:hover": {
        "marginTop": 1,
        "marginBottom": 1
    },
    "start-box button:active": {
        "marginTop": 3,
        "marginBottom": 3
    },
    "start-box button:first-child": {
        "borderRight": "1px solid #1a915a",
        "borderBottomRightRadius": 0,
        "borderTopRightRadius": 0
    },
    "start-box button:last-child": {
        "borderLeft": "1px solid #1a915a",
        "borderBottomLeftRadius": 0,
        "borderTopLeftRadius": 0
    },
    "start-box button name": {
        "fontSize": 25,
        "fontWeight": "bold",
        "marginTop": 3
    },
    "vote-box": {
        "position": "absolute",
        "top": 370,
        "left": 80,
        "paddingTop": 16,
        "paddingRight": 0,
        "paddingBottom": 16,
        "paddingLeft": 0,
        "border": "5px solid #23c478",
        "borderRadius": 5,
        "background": "#1e3c78",
        "width": 139,
        "textAlign": "center"
    },
    "vote-boxclinton": {
        "right": 80,
        "left": "auto",
        "borderColor": "#d41b45"
    },
    "vote-box span": {
        "fontSize": 38,
        "fontWeight": "bold"
    },
    "playlists": {
        "textAlign": "center"
    },
    "single content": {
        "textAlign": "center",
        "borderBottom": "1px solid #000",
        "backgroundColor": "#f2f8ff"
    },
    "player-container": {
        "fontWeight": "bold",
        "fontSize": 18,
        "lineHeight": 28,
        "paddingTop": 6,
        "paddingRight": 0,
        "paddingBottom": 6,
        "paddingLeft": 0
    },
    "player-container player-side": {
        "display": "inline-block",
        "width": 440
    },
    "player-container player-sideplayer-left": {
        "textAlign": "right"
    },
    "player-container player-sideplayer-right": {
        "textAlign": "left"
    },
    "player-container:after": {
        "display": "block",
        "marginTop": 10,
        "marginRight": "auto",
        "marginBottom": 10,
        "marginLeft": "auto",
        "content": "",
        "width": 500,
        "borderBottom": "1px solid #8fb9c9"
    },
    "player-container a": {
        "display": "inline-block"
    },
    "player-container span": {
        "display": "inline-block"
    },
    "player-container info": {
        "position": "relative"
    },
    "player-container info by": {
        "position": "absolute",
        "top": 15,
        "right": 0,
        "fontSize": 12,
        "fontWeight": "normal"
    },
    "player-container info by-clinton": {
        "left": 0,
        "right": "auto"
    },
    "player-container share": {
        "fontSize": 14,
        "fontWeight": "normal",
        "color": "inherit"
    },
    "player-container date": {
        "fontSize": 14,
        "fontWeight": "normal",
        "color": "inherit"
    },
    "player-container conversation-comments": {
        "fontSize": 14,
        "fontWeight": "normal",
        "color": "inherit"
    },
    "player-top": {
        "fontSize": 12,
        "height": 16,
        "fontWeight": "400"
    },
    "conversation-comments": {
        "color": "rgb(30, 60, 120) !important"
    },
    "conversation-comments a": {
        "color": "rgb(30, 60, 120) !important"
    },
    "fb-comments-count": {
        "fontWeight": "bold",
        "textDecoration": "underline"
    },
    "pointer": {
        "width": 81,
        "height": 42,
        "position": "absolute",
        "top": 51,
        "left": 436,
        "background": "transparent url(../img/share.png) 50% 50% no-repeat"
    },
    "sm2_button": {
        "height": 37,
        "paddingLeft": 35,
        "display": "inline-block",
        "background": "transparent url(../img/playerbtn-green.png) top left no-repeat",
        "textDecoration": "none",
        "color": "inherit",
        "marginLeft": 8
    },
    "sm2_buttonsm2_paused": {
        "paddingLeft": 40
    },
    "sm2_buttonsm2_playing": {
        "paddingLeft": 40,
        "background": "transparent url(../img/playerbtn-green-pause.png) top left no-repeat"
    },
    "pl": {
        "marginTop": 60
    },
    "pl list": {
        "marginTop": 28,
        "marginRight": 28,
        "marginBottom": 28,
        "marginLeft": 28
    },
    "more": {
        "color": "#8fb9c9",
        "textDecoration": "underline",
        "cursor": "pointer",
        "fontSize": 18
    },
    "more:hover": {
        "textDecoration": "none"
    },
    "player-container votes": {
        "border": "1px solid #8fb9c9",
        "borderRadius": 4,
        "fontWeight": "normal",
        "height": 30,
        "width": 64,
        "lineHeight": 24,
        "paddingTop": 2,
        "paddingRight": 0,
        "paddingBottom": 2,
        "paddingLeft": 11,
        "boxSizing": "border-box",
        "marginTop": 10,
        "marginRight": 5,
        "marginBottom": 0,
        "marginLeft": 5,
        "fontSize": 16,
        "background": "#FFF 9px 9px no-repeat",
        "cursor": "pointer",
        "textAlign": "center"
    },
    "player-container voteswinning": {
        "color": "#23c478",
        "position": "relative",
        "backgroundImage": "url(../img/vote-win.png)"
    },
    "player-container voteslosing": {
        "color": "#cf4e39",
        "position": "relative",
        "backgroundImage": "url(../img/vote-lose.png)"
    },
    "voteslosing::after": {
        "content": "'losing'",
        "fontSize": 10,
        "top": 24,
        "position": "absolute",
        "left": 15,
        "color": "#1e3c78",
        "opacity": 0.7
    },
    "voteswinning::after": {
        "content": "'winning'",
        "fontSize": 10,
        "top": 24,
        "position": "absolute",
        "left": 15,
        "opacity": 0.7,
        "color": "#1e3c78"
    },
    "player-container votesvoted": {
        "backgroundColor": "#8fb9c9",
        "color": "#FFF"
    },
    "callmaker": {
        "background": "#0a58c4",
        "color": "#FFF",
        "marginTop": 42,
        "paddingBottom": 10
    },
    "callmaker img": {
        "float": "left",
        "marginTop": 25,
        "marginRight": 26,
        "marginBottom": 0,
        "marginLeft": 36
    },
    "callmaker title": {
        "paddingTop": 30,
        "paddingRight": 0,
        "paddingBottom": 7,
        "paddingLeft": 0
    },
    "callmaker p": {
        "paddingTop": 4,
        "paddingRight": 0,
        "paddingBottom": 4,
        "paddingLeft": 0,
        "lineHeight": 23
    },
    "callmaker a": {
        "textDecoration": "none",
        "background": "#1ec476",
        "border": "3px solid transparent",
        "borderRadius": 50,
        "height": 50,
        "display": "inline-block",
        "marginTop": 10,
        "marginRight": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "width": 200,
        "textAlign": "center",
        "fontSize": 20,
        "lineHeight": 50,
        "borderLeftWidth": 0,
        "borderRightWidth": 0,
        "borderBottomColor": "#06a75b",
        "color": "#FFF",
        "textShadow": "1px 1px 2px rgba(0,0,0,.5)"
    },
    "callmaker a:hover": {
        "marginTop": 12,
        "borderBottomWidth": 2
    },
    "comments": {
        "background": "#063270",
        "paddingTop": 20,
        "paddingRight": 0,
        "paddingBottom": 20,
        "paddingLeft": 0
    },
    "links": {
        "background": "#063270",
        "paddingTop": 20,
        "paddingRight": 0,
        "paddingBottom": 20,
        "paddingLeft": 0,
        "textAlign": "center",
        "color": "#8ab0e5"
    },
    "comments-c": {
        "background": "#fff",
        "width": 750,
        "paddingTop": 10,
        "paddingRight": 10,
        "paddingBottom": 10,
        "paddingLeft": 10,
        "marginTop": 20,
        "marginRight": "auto",
        "marginBottom": 20,
        "marginLeft": "auto"
    },
    "comments h2": {
        "color": "#fff"
    },
    "links a": {
        "color": "#8ab0e5"
    },
    "links a:hover": {
        "opacity": 0.8
    },
    "start": {},
    "title-big": {
        "fontSize": 38,
        "color": "#1e3c78",
        "fontWeight": "bold",
        "marginTop": 20,
        "marginRight": 0,
        "marginBottom": 20,
        "marginLeft": 0
    },
    "title-text": {
        "textAlign": "center",
        "lineHeight": 24
    },
    "rules-header": {
        "marginTop": 30,
        "color": "#d41b45",
        "fontWeight": "bold",
        "fontSize": 18
    },
    "rules-ul": {
        "listStyle": "disc",
        "paddingTop": 20,
        "paddingLeft": 300
    },
    "rules-ul li": {
        "lineHeight": 22
    },
    "form-title": {
        "color": "#fff",
        "marginBottom": 10
    },
    "queueForm form-input": {
        "fontSize": 16,
        "background": "#fff",
        "borderRadius": 40,
        "width": "80%",
        "height": 40,
        "paddingLeft": 20,
        "border": "none",
        "outline": "none",
        "marginTop": 10,
        "marginRight": 0,
        "marginBottom": 10,
        "marginLeft": 0
    },
    "form-btn": {
        "width": "85%",
        "cursor": "pointer",
        "display": "inline-block",
        "textAlign": "center",
        "background": "#2BC47D",
        "marginTop": 10,
        "borderRadius": 30,
        "border": "none",
        "outline": "none",
        "height": 45,
        "color": "#fff",
        "fontWeight": "bold",
        "fontSize": 16,
        "lineHeight": 41,
        "letterSpacing": 1,
        "boxShadow": "inset 0 -3px 0 rgba(0,0,0,.15), 0 2px 1px rgba(0,0,0,.1)",
        "transition": "all 0.2s"
    },
    "form-btn:hover": {
        "boxShadow": "none"
    },
    "queueForm terms": {
        "color": "#fff",
        "fontSize": 10,
        "marginTop": 10
    },
    "radio-container input": {
        "display": "none"
    },
    "radio-container label": {
        "marginTop": 5,
        "marginRight": 10,
        "marginBottom": 5,
        "marginLeft": 10,
        "cursor": "pointer",
        "display": "inline-block",
        "border": "2px solid #4bc1aa",
        "borderRadius": 50,
        "paddingTop": 16,
        "paddingRight": 15,
        "paddingBottom": 16,
        "paddingLeft": 46,
        "textTransform": "uppercase",
        "fontWeight": "bold",
        "position": "relative",
        "fontSize": 14,
        "letterSpacing": 1.5,
        "opacity": 0.5
    },
    "radio-container input:checked + label": {
        "opacity": 1
    },
    "radio-container input:checked + label:before": {
        "backgroundPositionX": -20
    },
    "radio-container label:before": {
        "display": "inline-block",
        "content": "",
        "height": 20,
        "width": 20,
        "background": "transparent url(http://callmaker.net/wp-content/themes/callmaker/newmain/img/tariffs-radio.png) top left no-repeat",
        "position": "absolute",
        "top": 13,
        "left": 12
    },
    "search": {
        "display": "none"
    },
    "single content h1": {
        "paddingTop": 55,
        "paddingRight": 0,
        "paddingBottom": 14,
        "paddingLeft": 0
    },
    "single content share soc": {
        "width": 27,
        "height": 26,
        "background": "transparent url(../img/socials.png) left top no-repeat",
        "marginTop": 0,
        "marginRight": 4,
        "marginBottom": -7,
        "marginLeft": 4
    },
    "single content share soctw": {
        "backgroundPositionX": -27
    },
    "single content share socgp": {
        "backgroundPositionX": -54
    },
    "single content share": {
        "display": "inline-block",
        "color": "#8fb9c9",
        "fontSize": 18,
        "lineHeight": 29,
        "paddingTop": 19,
        "paddingRight": 0,
        "paddingBottom": 74,
        "paddingLeft": 0
    },
    "single content share a": {
        "display": "inline-block",
        "color": "#8fb9c9",
        "fontSize": 18,
        "lineHeight": 29
    },
    "single content share a:hover": {
        "textDecoration": "none"
    },
    "single header": {
        "backgroundColor": "#f2f8ff"
    },
    "pl leaderboard-playlist": {
        "marginTop": 64
    }
});