/**
 * Queue.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const CLINTON = "0"
const TRUMP = "1"

module.exports = {

    attributes: {
        matched: {
            type: 'boolean',
            defaultsTo: false
        },
        side: {type: 'string'},
        number: {type: 'string', required: true},
        conversation: {
            model: 'conversation'
        },
        nickname: {type: "string", required: true},
        subj: {type: "string", required: true},
        rates: {
            collection: 'rate',
            via: 'queue'
        }
    },

    afterCreate(queue, cb){
        let query = {
            side: queue.side == CLINTON ? TRUMP : CLINTON,
            matched: { '!': true }
        }

        Queue.findOne(query).exec((err, match) => {

            if(err)
                console.log(err)

            if(!match)
                return;

            Queue.update({id: queue.id}, {matched: true}).exec((err, updated) => console.log(err, updated))
            Queue.update({id: match.id}, {matched: true}).exec((err, updated) => console.log(err, updated))

            Conversation.create({
                queue: [queue, match]
            }).then(conv => {
            }).catch(err => {
                if(err)
                    console.log(err)
            })

        })
        cb()
    }
};
