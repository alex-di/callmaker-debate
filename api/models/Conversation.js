/**
 * Conversation.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        record : { type: 'string' },
        voiceData: {type: 'json'},
        first:{type:'string'},

        queue: {
            collection: 'queue',
            via: 'conversation'
        },

        rates: {
            collection: 'rate',
            via: 'conversation'
        },

        getVoiceData(){
            if(this.voiceData)
                return this.voiceData

            data = TwilioService.generateVoiceData({
                firstToTalk: 'clinton'
            })

            this.voiceData = data
            this.save()
            return data
        },
    },
    beforeCreate(conv,cb){
        console.log('before Create 1');
        conv.first = Math.random()>0.5?'1':'0';
        cb();
    },
    afterCreate(conv, cb){
        console.log("CONVERSATION CREATED ",conv);
        Conversation.findOne({id:conv.id}).populate("queue").exec(function(err,conv){
            TwilioService.initialize(conv);
            cb();
        });
    }
};
