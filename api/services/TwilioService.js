
const TWILIO_API = require('twilio')(sails.config.twilio.account, sails.config.twilio.auth)
const TWILIO = require('twilio');
const CLINTON = "0"
const TRUMP = "1"

var request = require('request');

module.TWILIO = TWILIO;
module.TWILIO_API = TWILIO_API;


process.on('uncaughtException', function(err) {
  console.log('BAD ERRROR',err);
});


module.exports = {
    initialize(options, done){
        console.log("Here to init twilio call", options);
        
        let trumpNumber = options.queue.find(el => el.side == TRUMP).number;
        let clintonNumber = options.queue.find(el => el.side == CLINTON).number;

        TWILIO_API.makeCall({
            url: `http://debatesclub.com/conference/${options.id}/conference.xml`,
            from: sails.config.twilio.callerid,
            to: trumpNumber,
            ifMachine:'Hangup',
            record:true
        });
        TWILIO_API.makeCall({
            url: `http://debatesclub.com/conference/${options.id}/conference.xml`,
            from: sails.config.twilio.callerid,
            to: clintonNumber,
            ifMachine:'Hangup',
            record:true
        });

        if(done)
            done()
    },

    conferenceResponse(options,cb){
        let resp = new TWILIO.TwimlResponse();
        console.log('conference BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBb ');
        resp.say('Please wait for your opponent to connect.',{loop:1}).dial({
            action:`http://debatesclub.com/conference/${options.id}/start.xml`
        }, function(node) {
            node.conference(`Debate-${options.id}`, {
                beep:'false',
                statusCallback:`http://debatesclub.com/conference/${options.id}/start.xml`,
                statusCallbackEvent:'start',
                maxParticipants:'3'
            })
        } );
        // console.log('ECHO ',resp.toString());
        cb({
            responseString:resp.toString()
        });
    },
    conferenceStartResponse(options,cb){
        console.log('conferenceStart///////////////////////////////////////////////////////// ');

        Conversation.findOne({id:options.id}).populate("queue").exec(function(err,conv){
            if(!conv || !conv.first){
                return;
            }
            console.log("CONVERSATION FOUND ",conv.first);
            let timeout = 2000;
            let debateLength = 30000;

            var unmuteEveryone = function(){
                participants.forEach(function(participant) {
                    TWILIO_API.conferences(options.ConferenceSid).participants(participant.callSid).update({
                        hold:"False"
                    },function(err,participant){
                        console.log('updated P ',participant);
                    });
                });
            };

            let participants = [];
            setTimeout(function(){
                console.log('BOT GO!!!!!!!!');
                TWILIO_API.conferences(options.ConferenceSid).participants.list(function(err, data) {
                    participants = data.participants;
                    participants.forEach(function(participant) {
                        console.log('participant ',participant);
                        TWILIO_API.conferences(options.ConferenceSid).participants(participant.callSid).update({
                            hold:"True",
                            holdMethod:"POST",
                            holdUrl:`http://debatesclub.com/conference/${options.id}/botsay.xml?action=start&first=${conv.first}`
                        },function(err,participant){
                            console.log('updated P ',participant);
                            console.log(`CALLBACK http://debatesclub.com/conference/${options.id}/botsay.xml`);
                        });
                    });
                });
            },timeout);

            timeout += 5000;
            //PREVIEW FINISHED
            setTimeout(function(){
                unmuteEveryone();
            },timeout);


            timeout += debateLength;
            //SECOND START
            setTimeout(function(){
                console.log('BOT GO!!!!!!!!');
                TWILIO_API.conferences(options.ConferenceSid).participants.list(function(err, data) {
                    participants = data.participants;
                    participants.forEach(function(participant) {
                        console.log('participant ',participant);
                        TWILIO_API.conferences(options.ConferenceSid).participants(participant.callSid).update({
                            hold:"True",
                            holdMethod:"POST",
                            holdUrl:`http://debatesclub.com/conference/${options.id}/botsay.xml?action=second&first=${conv.first}`
                        },function(err,participant){
                            console.log('updated P ',participant);
                            console.log(`CALLBACK http://debatesclub.com/conference/${options.id}/botsay.xml`);
                        });
                    });
                });

            },timeout);        

            timeout += 5000;
            //SECOND FINISH
            setTimeout(function(){
                unmuteEveryone();
            },timeout);



            timeout += debateLength;
            //THIRD START
            setTimeout(function(){
                console.log('BOT GO!!!!!!!!');
                TWILIO_API.conferences(options.ConferenceSid).participants.list(function(err, data) {
                    participants = data.participants;
                    participants.forEach(function(participant) {
                        console.log('participant ',participant);
                        TWILIO_API.conferences(options.ConferenceSid).participants(participant.callSid).update({
                            hold:"True",
                            holdMethod:"POST",
                            holdUrl:`http://debatesclub.com/conference/${options.id}/botsay.xml?action=third&first=${conv.first}`
                        },function(err,participant){
                            console.log('updated P ',participant);
                            console.log(`CALLBACK http://debatesclub.com/conference/${options.id}/botsay.xml`);
                        });
                    });
                });

            },timeout);        

            timeout += 5000;
            //THIRD FINISH
            setTimeout(function(){
                unmuteEveryone();
            },timeout);



            timeout += debateLength;
            //FOURTH START
            setTimeout(function(){
                console.log('BOT GO!!!!!!!!');
                TWILIO_API.conferences(options.ConferenceSid).participants.list(function(err, data) {
                    participants = data.participants;
                    participants.forEach(function(participant) {
                        console.log('participant ',participant);
                        TWILIO_API.conferences(options.ConferenceSid).participants(participant.callSid).update({
                            hold:"True",
                            holdMethod:"POST",
                            holdUrl:`http://debatesclub.com/conference/${options.id}/botsay.xml?action=fourth&first=${conv.first}`
                        },function(err,participant){
                            console.log('updated P ',participant);
                            console.log(`CALLBACK http://debatesclub.com/conference/${options.id}/botsay.xml`);
                        });
                    });
                });

            },timeout);        

            timeout += 5000;
            //SECOND FINISH
            setTimeout(function(){
                unmuteEveryone();
            },timeout);



            timeout += debateLength;
            setTimeout(function(){
                console.log('BOT GO!!!!!!!!');
                TWILIO_API.conferences(options.ConferenceSid).participants.list(function(err, data) {
                    participants = data.participants;
                    participants.forEach(function(participant) {
                        console.log('participant ',participant);
                        TWILIO_API.conferences(options.ConferenceSid).participants(participant.callSid).update({
                            hold:"True",
                            holdMethod:"POST",
                            holdUrl:`http://debatesclub.com/conference/${options.id}/botsay.xml?action=final&first=${conv.first}`
                        },function(err,participant){
                            console.log('updated P ',participant);
                            console.log(`CALLBACK http://debatesclub.com/conference/${options.id}/botsay.xml`);
                        });
                    });
                });

            },timeout);

            timeout += 5000;        
            setTimeout(function(){
                participants.forEach(function(participant,i) {
                    TWILIO_API.calls(participant.callSid).update({
                        status: "completed"
                    }, function(err, call) {
                        console.log(call.direction);
                    });
                
                    console.log('i = ',i);
                    if(i==0){
                        //Writing recording
                        setTimeout(function(){
                            TWILIO_API.recordings.list({ callSid: participant.callSid }, function(err, data) {
                                data.recordings.forEach(function(recording) {
                                    console.log('RECORDING! ',recording);
                                    conv.record = 'http://debatesclub.com/recordings/'+recording.sid+'.mp3';
                                    conv.save();
                                });
                            });
                        },1000);
                    }

                });
            },timeout);


            console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ',timeout);


            cb({});
        });
        /*
        TWILIO_API.conferences(options.ConferenceSid).participants.list(function(err, data) {
            data.participants.forEach(function(participant) {
                console.log(participant);
            });
        });
        */
    },
    conferenceBotSayResponse(options,cb){
        let resp = new TWILIO.TwimlResponse();
        console.log('conferenceOptions ',options);
        if(options.action=='start'){
            if(options.first==0){
                resp.say('Donald Trump supporter starts first.');
            }else{
                resp.say('Hillary Clinton supporter starts first.');
            }
        }else if(options.action=='second'){
            if(options.first==0){
                resp.say('Now it is time for Сlinton supporter.');
            }else{
                resp.say('Now it is time for Trump supporter.');
            }   
        }else if(options.action=='third'){
            if(options.first==0){
                resp.say('Time for Trump supporter!');
            }else{
                resp.say('Time for Clinton supporter');
            }   
        }else if(options.action=='fourth'){
            if(options.first==0){
                resp.say('Time for Clinton supporter');
            }else{
                resp.say('Time for Trump supporter!');
            }   
        }else{
            resp.say('Thank you for the debate!');
        }
        console.log('ECHO ',resp.toString());
        cb({
            responseString:resp.toString()
        });
    },
    generateVoiceData(options){

        let resp = new TWILIO.TwimlResponse();
        resp.say('Welcome to Twilio!');
        return resp.toString()

    },
    recordings(req,res){
        /*
        TWILIO_API.recordings.list(function(err, data) {
            data.recordings.forEach(function(recording) {
                console.log(recording);
            });
        });
        */
    }, recording(req,res,options){
        options.id = options.id.replace('.mp3','');
        TWILIO_API.recordings(options.id).get(function(err, recording) {
            var recordUrl = 'https://api.twilio.com/'+recording.uri.replace('.json','.mp3');
            req.pipe(request(recordUrl,{
                'auth': {
                    'user': sails.config.twilio.account,
                    'pass':sails.config.twilio.auth
                }   
            })).pipe(res);
        });
    }
}
