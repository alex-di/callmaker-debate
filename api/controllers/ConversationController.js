/**
 * ConversationController
 *
 * @description :: Server-side logic for managing conversations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment = require('moment')

module.exports = {

	single(req, res, next){
		Conversation.findOne({id: req.params.id})
		.populate('queue')
		.populate('rates')
		.then((conv) => {

			console.log('conv', conv)
			Object.assign(conv, {
				title: moment(conv.createdAt).format('ll'),
                TRUMP: conv.queue.find((e) => e.side == '1'),
                CLINTON: conv.queue.find((e) => e.side == '0'),
			})

			conv.TRUMP.rates = 0
			conv.CLINTON.rates = 0

			conv.rates.forEach(function(r){
				if(r.queue == conv.CLINTON.id){
					conv.CLINTON.rates++
				} else {
					conv.TRUMP.rates++
				}
			})

			// console.log(conv)
			// res.send(404)
			res.view('single', {conv: conv})
		})
	},

	total(req, res){
		Rate.native(function(err, col){
			col.aggregate([
				{$lookup: {
					from: 'queue',
					localField: 'queue',
					foreignField: '_id',
					as: "queue"
				}},
				{$group: {
					_id: '$queue.side',
					total: {$sum: 1}
				}},
			]).toArray(function(err, result){

				console.log(err)
				res.json(result);
			})
		})
	},

	leaderboard(req, res){


		Rate.native(function(err, col){
			col.aggregate([
				{$group: {
					_id: '$conversation',
					rating: {$sum: 1}
				}},
				{$sort: {
					rating: -1
				}},
				{$skip: parseInt(req.query.skip) || 0},
				{$limit: 5}
			]).toArray(function(err, result){
				var map = {}
				result.forEach(r => map[r._id] = r.rating)
				Conversation.find({
					id: result.map(el => el._id)
				})
				.populate('queue')
				.populate('rates')
				.then((convs) => {
					res.json(convs.sort((a, b) => {
						return map[b.id] - map[a.id]
					}))


				})
				.catch((err) => {console.log(err); res.send(500, err)})
			})
		})
	},
	newest(req, res){
		Conversation.find({
			record: /.+/
		})
		.limit(5)
		.skip(req.query.skip)
		.sort('createdAt DESC')

		.populate('queue')
		.populate('rates')
		.then((convs) => res.json(convs)).catch((err) => {console.log(err); res.send(500, err)})
	},
	conference(req,res){
		console.log('conference ',req.params.id);
		TwilioService.conferenceResponse({id:req.params.id},function(resp){
			res.writeHead(200, {'Content-Type': 'text/xml'});
			res.end(resp.responseString);
		});
	},
	conferenceStart(req,res){
		// console.log('conference started ',req.body);
		TwilioService.conferenceStartResponse({id:req.params.id, ConferenceSid:req.body.ConferenceSid},function(resp){
			console.log("resp ",resp);
			res.writeHead(200);
			res.end();
		});
	},botSay(req,res){
		console.log('BOTSAY ',req.body,req.query);
		TwilioService.conferenceBotSayResponse({id:req.params.id, action: req.query.action, first:req.query.first},function(resp){
			console.log("resp ",resp);
			res.writeHead(200, {'Content-Type': 'text/xml'});
			res.end(resp.responseString);
		});
	},
	recordings(req,res){
		TwilioService.recordings(req,res);
	},
	recording(req,res){
		console.log('recording 1');
		TwilioService.recording(req,res,{id:req.params.id});
	}
};
